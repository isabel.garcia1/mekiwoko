/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-29-2022
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@isTest public with sharing class Setup_nextVaccinationTest {

    static Specie__c createSpecie(){
        Specie__c specie = new Specie__c(Name='Galgo', Vaccination_period__c=5);
        insert specie;
        return specie;
    }
      

    static void createPetNull(Specie__c specie){
        Pet__c pet=new Pet__c(Name='Pepa',Specie__c=specie.Id); 
        insert pet;
    }

    static void createPet(Specie__c specie){
        Pet__c pet=new Pet__c(Name='Pepa',Specie__c=specie.Id,First_Vaccine__c=Date.today()); 
        insert pet;
    }

    public static void generateData(){
        Specie__c specie=createSpecie();
        createPet(specie);
        createPetNull(specie);
    }
}