/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-29-2022
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@isTest public with sharing class nextVaccinationTest {
    
    @TestSetup static void nextVaccinationTest1(){
        Setup_nextVaccinationTest.generateData();
    }

    @isTest static void test1(){
        test.startTest();
        Setup_nextVaccinationTest.generateData();
        test.stopTest();
    }
}