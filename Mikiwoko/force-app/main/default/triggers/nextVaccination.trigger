/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-28-2022
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
trigger nextVaccination on Pet__c (before insert, before update) {
    //List of Specie__c 
    Set<Id> specieIdSet= new Set<Id>();
    for (Pet__c pet: Trigger.new){
        specieIdSet.add(pet.Specie__c);	
    }  
    //Relaciona Id de Specie__c con List de Vaccination_period__c  
    Map<Id,Specie__c> mapaSpecie=new Map<Id,Specie__c>([SELECT Id,Vaccination_period__c FROM Specie__c WHERE Id in :specieIdSet]);

    for(Pet__c petP: Trigger.new){
        if(petP.First_Vaccine__c==null){
            petP.First_Vaccine__c=Date.today();
        }
        Specie__c speciePet= mapaSpecie.get(petP.Specie__c);
        Decimal vacc= speciePet.Vaccination_period__c;
        petP.Next_Vaccine__c=petP.First_Vaccine__c.addMonths(vacc.intValue());
    }
}